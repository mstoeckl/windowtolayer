#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Read protocol files, and convert to a rust-readable format
"""

import os
import collections
import xml.etree.ElementTree

header = """/* Code automatically generated from protocols/ folder */
use crate::wayland_util::*;
use WaylandArgument::*;
"""


def unsnake(s):
    return "".join(map(lambda x: x.capitalize(), s.split("_")))


def clean_name(s):
    return s.strip("_")


def snake_case(s):
    return "".join(("_" if c.isupper() else "") + c.lower() for c in s)


def get_signature(method):
    """
    returns a string like "[Int, Uint, NewId(WlOutput)]"
    """
    vals = []
    for arg in method:
        if arg.tag != "arg":
            continue
        if arg.attrib["type"] == "object":
            if "interface" in arg.attrib:
                vals.append(
                    "Object(&" + clean_name(arg.attrib["interface"]).upper() + ")"
                )
            else:
                vals.append("GenericObject")
        elif arg.attrib["type"] == "new_id":
            if "interface" in arg.attrib:
                vals.append(
                    "NewId(&" + clean_name(arg.attrib["interface"]).upper() + ")"
                )
            else:
                vals.append("GenericNewId")
        elif arg.attrib["type"] == "int":
            vals.append("Int")
        elif arg.attrib["type"] == "uint":
            vals.append("Uint")
        elif arg.attrib["type"] == "fixed":
            vals.append("Fixed")
        elif arg.attrib["type"] == "string":
            vals.append("String")
        elif arg.attrib["type"] == "array":
            vals.append("Array")
        elif arg.attrib["type"] == "fd":
            vals.append("Fd")
        else:
            raise NotImplementedError(arg.attrib)
    return "&[" + ", ".join(vals) + "]"


def write_method_length(meth_name, method, write):
    """
    Create a function to report how long the method would be
    """
    lines = []
    args = []
    base_len = 8
    for arg in method:
        if arg.tag != "arg":
            continue
        name = snake_case(arg.attrib["name"])
        if arg.attrib["type"] == "new_id":
            if "interface" in arg.attrib:
                base_len += 4
            else:
                arg_name = name + "_iface_name"
                args.append(arg_name + ": &[u8]")
                lines.append("    v += length_string(Some({}));".format(arg_name))
                base_len += 8
        elif arg.attrib["type"] in ("int", "uint", "object", "fixed"):
            base_len += 4
        elif arg.attrib["type"] == "string":
            args.append(name + ": Option<&[u8]>")
            lines.append("    v += length_string({});".format(name))
        elif arg.attrib["type"] == "array":
            args.append(name + ": &[u8]")
            lines.append("    v += length_array({});".format(name))
        elif arg.attrib["type"] == "fd":
            pass
        else:
            raise NotImplementedError(arg.attrib)

    write("#[allow(dead_code)]")
    write("pub fn length_{}({}) -> usize {{".format(meth_name, ", ".join(args)))
    if lines:
        write("    let mut v = {};".format(base_len))
        for l in lines:
            write(l)
        write("    v")
    else:
        write("    {}".format(base_len))
    write("}")


def write_method_write(meth_name, meth_num, method, write):
    """
    Create a function to write the method to a buffer
    """
    objtype = "DownstreamID" if method.tag == "event" else "UpstreamID"

    length_args = []
    for arg in method:
        if arg.tag != "arg":
            continue
        name = snake_case(arg.attrib["name"])
        if arg.attrib["type"] == "new_id":
            if "interface" not in arg.attrib:
                length_args.append(name + "_iface_name")
        elif arg.attrib["type"] == "string":
            length_args.append(name)
        elif arg.attrib["type"] == "array":
            length_args.append(name)
        elif arg.attrib["type"] in ("fd", "uint", "int", "object", "fixed"):
            pass
        else:
            raise NotImplementedError(arg.attrib)

    args = [("dst", "&mut &mut [u8]"), ("for_id", objtype)]
    lines = [
        "    let l = length_{}({});".format(meth_name, ", ".join(length_args)),
        "    if dst.len() < l {",
        "        return None;",
        "    }",
        "    write_header(dst, for_id.0, l, {})?;".format(meth_num),
    ]

    base_len = 8
    for arg in method:
        if arg.tag != "arg":
            continue
        name = snake_case(arg.attrib["name"])
        if arg.attrib["type"] == "object":
            args.append((name, objtype))
            lines.append("    write_u32(dst, {}.0)?;".format(name))
        elif arg.attrib["type"] == "new_id":
            if "interface" in arg.attrib:
                args.append((name, objtype))
                lines.append("    write_u32(dst, {}.0)?;".format(name))
            else:
                args.append((name + "_iface_name", "&[u8]"))
                args.append((name + "_version", "u32"))
                args.append((name, objtype))
                lines.append(
                    "    write_string(dst, Some({}))?;".format(name + "_iface_name")
                )
                lines.append("    write_u32(dst, {})?;".format(name + "_version"))
                lines.append("    write_u32(dst, {}.0)?;".format(name))
        elif arg.attrib["type"] == "int":
            args.append((name, "i32"))
            lines.append("    write_i32(dst, {})?;".format(name))
        elif arg.attrib["type"] == "uint":
            args.append((name, "u32"))
            lines.append("    write_u32(dst, {})?;".format(name))
        elif arg.attrib["type"] == "fixed":
            args.append((name, "i32"))
            lines.append("    write_i32(dst, {})?;".format(name))
        elif arg.attrib["type"] == "string":
            args.append((name, "Option<&[u8]>"))
            lines.append("    write_string(dst, {})?;".format(name))
        elif arg.attrib["type"] == "array":
            args.append((name, "&[u8]"))
            lines.append("    write_array(dst, {})?;".format(name))
        elif arg.attrib["type"] == "fd":
            pass
        else:
            raise NotImplementedError(arg.attrib)

    lines.append("    Some(())")
    write("#[allow(dead_code)]")
    write(
        "fn try_write_{}({}) -> Option<()> {{".format(
            meth_name, ", ".join([x + ": " + y for x, y in args])
        )
    )
    for l in lines:
        write(l)
    write("}")

    write("#[allow(dead_code)]")
    write(
        "pub fn write_{}({}) {{".format(
            meth_name, ", ".join([x + ": " + y for x, y in args])
        )
    )
    write(
        "    try_write_{}({}).unwrap();".format(
            meth_name, ", ".join([x for x, y in args])
        )
    )
    write("}")


def write_method_parse(meth_name, method, write):
    """
    Create a function to parse the method tail
    """
    objtype = "UpstreamID" if method.tag == "event" else "DownstreamID"

    length_args = []
    for arg in method:
        if arg.tag != "arg":
            continue
        name = snake_case(arg.attrib["name"])
        if arg.attrib["type"] == "new_id":
            if "interface" not in arg.attrib:
                length_args.append(name + "_iface_name")
        elif arg.attrib["type"] == "string":
            length_args.append(name)
        elif arg.attrib["type"] == "array":
            length_args.append(name)
        elif arg.attrib["type"] in ("fd", "uint", "int", "object", "fixed"):
            pass
        else:
            raise NotImplementedError(arg.attrib)

    sig = []
    lines = []
    ret = []

    for arg in method:
        if arg.tag != "arg":
            continue
        name = snake_case(arg.attrib["name"])
        if arg.attrib["type"] == "object":
            lines.append("    let {} = {}(parse_u32(&mut msg)?);".format(name, objtype))
            sig.append(objtype)
            ret.append(name)
        elif arg.attrib["type"] == "new_id":
            if "interface" in arg.attrib:
                lines.append(
                    "    let {} = {}(parse_u32(&mut msg)?);".format(name, objtype)
                )
                ret.append(name)
                sig.append(objtype)
            else:
                lines.append(
                    "    let {}_iface_name = parse_string(&mut msg)?;".format(name)
                )
                lines.append("    let {}_version = parse_u32(&mut msg)?;".format(name))
                lines.append(
                    "    let {} = {}(parse_u32(&mut msg)?);".format(name, objtype)
                )
                sig.append("Option<&'a [u8]>")
                sig.append("u32")
                sig.append(objtype)
                ret.append("{}_iface_name".format(name))
                ret.append("{}_version".format(name))
                ret.append(name)
        elif arg.attrib["type"] == "int":
            lines.append("    let {} = parse_i32(&mut msg)?;".format(name))
            sig.append("i32")
            ret.append(name)
        elif arg.attrib["type"] in ("uint", "fixed"):
            lines.append("    let {} = parse_u32(&mut msg)?;".format(name))
            sig.append("u32")
            ret.append(name)
        elif arg.attrib["type"] == "string":
            lines.append("    let {} = parse_string(&mut msg)?;".format(name))
            sig.append("Option<&'a [u8]>")
            ret.append(name)
        elif arg.attrib["type"] == "array":
            lines.append("    let {} = parse_array(&mut msg)?;".format(name))
            sig.append("&'a [u8]")
            ret.append(name)
        elif arg.attrib["type"] == "fd":
            pass
        else:
            raise NotImplementedError(arg.attrib)

    uscore = "" if not sig else "mut "
    paren = (lambda x: "(" + x + ")") if len(sig) != 1 else (lambda x: x)
    write("#[allow(dead_code)]")
    write(
        "pub fn parse_{}<'a>({}msg: &'a [u8]) -> Result<{}, ParseError> {{".format(
            meth_name, uscore, paren(", ".join(sig))
        )
    )
    if sig:
        write("    msg = msg.get(8..).ok_or(ParseError(()))?;")
        for l in lines:
            write(l)
        write("    if !msg.is_empty() { return Err(ParseError(())); }")
    else:
        write("    if msg.len() != 8 { return Err(ParseError(())); }")
    write("    Ok(" + paren(", ".join(ret)) + ")")
    write("}")


def write_enum(enum_name, enum_entries, parse_name, write):
    write("#[allow(dead_code)]")
    write("pub enum " + enum_name + " {")
    for i, name in enumerate(enum_entries):
        write("    " + name + " = " + str(i) + ",")
    write("}")
    write("#[allow(dead_code)]")
    write("pub fn parse_" + parse_name + "(v: u32) -> Option<" + enum_name + "> {")
    write("    match v {")
    for i, name in enumerate(enum_entries):
        write("        {} => Some({}::{}),".format(i, enum_name, name))
    write("        _ => None,")
    write("    }")
    write("}")


def process_interface(interface, uid, write):
    req_counter = 0
    evt_counter = 0
    iface_name_raw = interface.attrib["name"]
    iface_name = clean_name(iface_name_raw)
    iface_version = interface.attrib["version"]
    evts = []
    reqs = []
    for thing in interface:
        if thing.tag == "event" or thing.tag == "request":
            is_event = thing.tag == "event"
            signature = get_signature(thing)
            destructor = "type" in thing.attrib and thing.attrib["type"] == "destructor"
            dst = evts if is_event else reqs
            meth_num = len(dst)
            dst.append((snake_case(thing.attrib["name"]), signature, destructor))

            meth_name = (
                clean_name(iface_name)
                + "_"
                + ("evt" if is_event else "req")
                + "_"
                + snake_case(thing.attrib["name"])
            )
            write_method_write(meth_name, meth_num, thing, write)
            write_method_length(meth_name, thing, write)
            write_method_parse(meth_name, thing, write)

        elif thing.tag == "enum":
            enum_name = iface_name + "_" + snake_case(thing.attrib["name"])
            write("#[allow(dead_code)]")
            write("#[derive(Copy, Clone)]")
            write("pub enum " + unsnake(enum_name) + " {")
            for elt in thing:
                if elt.tag == "entry":
                    name = unsnake(elt.attrib["name"])
                    if name.isnumeric():
                        name = "Item" + name
                    write("    " + name + " = " + elt.attrib["value"] + ",")
            write("}")

    write("#[allow(dead_code)]")
    write(
        "pub static " + iface_name.upper() + ": WaylandInterface = WaylandInterface {"
    )

    write('    name: "' + iface_name_raw + '",')
    if evts:
        write("    evts: &[")
        for name, sig, destructor in evts:
            write("        WaylandMethod {")
            write('            name: "' + name + '",')
            write("            sig: " + sig + ",")
            write("            destructor: " + str(destructor).lower() + ",")
            write("        },")
        write("    ],")
    else:
        write("    evts: &[],")
    if reqs:
        write("    reqs: &[")
        for name, sig, destructor in reqs:
            write("        WaylandMethod {")
            write('            name: "' + name + '",')
            write("            sig: " + sig + ",")
            write("            destructor: " + str(destructor).lower() + ",")
            write("        },")
        write("    ],")
    else:
        write("    reqs: &[],")
    write("    version: " + iface_version + ",")
    write("    uid: " + str(uid) + ",")
    write("};")

    if evts:
        write_enum(
            unsnake(iface_name) + "EvtIDs",
            [unsnake(name) for name, _, _ in evts],
            iface_name + "_evt_ids",
            write,
        )
    if reqs:
        write_enum(
            unsnake(iface_name) + "ReqIDs",
            [unsnake(name) for name, _, _ in reqs],
            iface_name + "_req_ids",
            write,
        )

    return (iface_name.upper(), iface_name_raw)


if __name__ == "__main__":
    import sys, subprocess

    if len(sys.argv) < 3:
        print("Usage: ./protogen.py output-file.rs [xml-files]", file=sys.stderr)
        quit()
    output_file = sys.argv[1]
    protocols = sorted(sys.argv[2:])
    if not all(map(lambda x: x.endswith(".xml"), protocols)):
        print("Not all input protocol files have .xml endings:", protocols)
        quit()

    with open(output_file, "w") as output:

        def write(*x):
            print(*x, file=output)

        write(header)

        interfaces = []
        uid = 0
        for protocol_file in protocols:
            root = xml.etree.ElementTree.parse(protocol_file).getroot()
            for interface in root:
                if interface.tag == "interface":
                    interfaces.append(process_interface(interface, uid, write))
                    uid += 1
        assert len(interfaces) == len(set(interfaces)), [
            (k, v) for k, v in collections.Counter(sorted(interfaces)).items() if v > 1
        ]

        write("pub static ALL_INTERFACES : &[&'static WaylandInterface] = &[")
        for intf, intf_raw in sorted(interfaces, key=lambda x: x[1]):
            write("    &" + intf + ",")
        write("];")

    subprocess.call(["rustfmt", output_file])
