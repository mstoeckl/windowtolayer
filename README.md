# windowtolayer

This program transforms individual Wayland clients, which use the xdg-shell
protocol to display windows, into clients that use the wlr-layer-shell protocol
to render as a wallpaper. Note that wlr-layer-shell is only supported by some
compositors (like KWin, and those based on wlroots.)

Example usage:

```
windowtolayer termite -e asciiquarium
```

## Building

Run `cargo build --release`; the executable will be in
`target/release/windowtolayer`.

The build script uses Python to generate Wayland protocol handling code.

## Status

This has many bugs and incompletely implemented message handlers, so certain
features (like opening popups) may not work.

An unavoidable limitation of the program design is a slight increase in latency
for clients run under `windowtolayer`.

## More examples

```
windowtolayer --output-name=DP-2 alacritty -e neo-matrix
windowtolayer --layer=top timeout 1 eglgears_wayland
windowtolayer --interactivity=all cage /usr/lib/xscreensaver/polyhedra
```
