/* SPDX-License-Identifier: GPL-3.0-or-later */
use arrayvec::ArrayVec;
use lexopt::{Arg, ValueExt};
use log::{debug, Log, Record};
use nix::sys::socket::{SockFlag, UnixAddr};
use nix::{fcntl, sys};
use std::collections::{BTreeMap, BTreeSet};
use std::ffi::{OsStr, OsString};
use std::fmt::{Display, Write as FmtWrite};
use std::io::{IoSlice, IoSliceMut, Write as IoWrite};
use std::os::fd::{AsFd, AsRawFd, FromRawFd, OwnedFd};
use std::process::Command;
use std::sync::Mutex;
use std::{env, os::fd::RawFd};
mod wayland;
mod wayland_util;
use crate::wayland::*;
use crate::wayland_util::*;

struct Logger {
    max_level: log::LevelFilter,
    cache: Mutex<String>,
}

static LOGGER: Logger = Logger {
    max_level: log::LevelFilter::Debug,
    /* A resizable cache to avoid allocating a new string on every debug message;
     * The maximum debug message size is bounded and is certainly <1MB, so this could
     * in theory be statically allocated. Since this program is single threaded (and
     * already grabs the stderr Mutex) the cost of this Mutex is negligible. */
    cache: Mutex::new(String::new()),
};

impl Log for Logger {
    fn enabled(&self, meta: &log::Metadata<'_>) -> bool {
        meta.level() <= self.max_level
    }
    fn log(&self, record: &Record<'_>) {
        if record.level() > self.max_level {
            return;
        }

        let time = std::time::SystemTime::now().duration_since(std::time::UNIX_EPOCH);
        let t = if let Ok(t) = time {
            (t.as_nanos() % 100000000000u128) / 1000u128
        } else {
            0
        };
        let pid = std::process::id();

        let mut cache = self.cache.lock().unwrap();

        writeln!(
            &mut cache,
            "[{:02}.{:06} windowtolayer({}) {}:{}] {}",
            t / 1000000u128,
            t % 1000000u128,
            pid,
            record
                .file()
                .unwrap_or("src/unknown")
                .strip_prefix("src/")
                .unwrap(),
            record.line().unwrap_or(0),
            record.args(),
        )
        .unwrap();
        /* Write in one chunk; for small writes this ensures atomicity of messages
         * in the combined log stream of this and other applications */
        let handle = &mut std::io::stderr().lock();
        let _ = handle.write_all(cache.as_bytes());
        let _ = handle.flush();

        /* Empty string but do not deallocate it*/
        cache.clear();
    }
    fn flush(&self) {
        /* not needed */
    }
}

fn connect_to_unix_addr(upstream_fd: OwnedFd, addr: &UnixAddr) -> Result<OwnedFd, &'static str> {
    let res = nix::sys::socket::connect(upstream_fd.as_raw_fd(), addr);
    if res.is_ok() {
        Ok(upstream_fd)
    } else {
        Err("Failed to connect to WAYLAND_DISPLAY")
    }
}

fn connect_to_upstream() -> Result<OwnedFd, &'static str> {
    let env_disp = env::var_os("WAYLAND_DISPLAY");
    let env_sock = env::var_os("WAYLAND_SOCKET");
    let env_dir = env::var_os("XDG_RUNTIME_DIR");
    if let Some(sock) = env_sock {
        if let Some(fd) = sock.into_string().ok().and_then(|x| x.parse::<u16>().ok()) {
            // Capture inherited fd from environment variable
            // Must only call connect_to_upstream() once, at risk of closing random fd
            let upstream_fd = unsafe { OwnedFd::from_raw_fd(RawFd::from(fd)) };

            // todo: this validates the number is a file descriptor, and should error if invalid
            if set_cloexec(&upstream_fd, true) {
                Ok(upstream_fd)
            } else {
                Err("WAYLAND_SOCKET did not indicate a valid file descriptor")
            }
        } else {
            Err("Failed to parse WAYLAND_SOCKET")
        }
    } else if let Some(disp) = env_disp {
        let leading_slash: &[u8] = b"/";

        let upstream_fd = nix::sys::socket::socket(
            nix::sys::socket::AddressFamily::Unix,
            nix::sys::socket::SockType::Stream,
            SockFlag::SOCK_NONBLOCK,
            None,
        )
        .unwrap();
        set_cloexec(&upstream_fd, true);

        if disp.as_encoded_bytes().starts_with(leading_slash) {
            let addr = UnixAddr::new(disp.as_os_str()).unwrap();
            connect_to_unix_addr(upstream_fd, &addr)
        } else if let Some(dir) = env_dir {
            let mut path = OsString::new();
            path.push(dir);
            path.push(OsString::from("/"));
            path.push(disp);
            let addr = UnixAddr::new(path.as_os_str()).unwrap();
            connect_to_unix_addr(upstream_fd, &addr)
        } else {
            Err("XDG_RUNTIME_DIR was not in environment")
        }
    } else {
        Err("Neither WAYLAND_DISPLAY nor WAYLAND_SOCKET available")
    }
}

fn set_cloexec(fd: &OwnedFd, cloexec: bool) -> bool {
    fcntl::fcntl(
        fd.as_raw_fd(),
        fcntl::FcntlArg::F_SETFD(if cloexec {
            nix::fcntl::FdFlag::FD_CLOEXEC
        } else {
            nix::fcntl::FdFlag::empty()
        }),
    )
    .is_ok()
}

fn read_from_socket(socket: &OwnedFd, bufs: &mut ProxySide) -> Result<(), &'static str> {
    // assume the socket starts _empty_, for now
    if bufs.nbytes_src == bufs.buf_src.len() {
        panic!(
            "no remaining space: {} used, {} total",
            bufs.nbytes_src,
            bufs.buf_src.len()
        );
    }
    assert!(bufs.fds.is_empty());

    let mut iovs = [IoSliceMut::new(&mut bufs.buf_src[bufs.nbytes_src..])];
    let mut cmsg_fds = nix::cmsg_space!([RawFd; 32]);

    // todo later: parse into chunks
    let r = nix::sys::socket::recvmsg::<UnixAddr>(
        socket.as_raw_fd(),
        &mut iovs,
        Some(&mut cmsg_fds),
        nix::sys::socket::MsgFlags::empty(),
    );
    // TODO: propagate errors
    match r {
        Ok(resp) => {
            bufs.nbytes_src += resp.bytes;
            for msg in resp.cmsgs().expect("Failed to get cmsgs") {
                match msg {
                    sys::socket::ControlMessageOwned::ScmRights(tfds) => {
                        bufs.fds
                            .extend(tfds.into_iter().map(|x| unsafe { OwnedFd::from_raw_fd(x) }));
                    }
                    _ => {
                        /* ignore? */
                        eprintln!("Received unexpected control message: {:?}", msg);
                        // TODO: error when unexpected control message arrives
                    }
                }
            }

            Ok(())
        }
        Err(nix::errno::Errno::EINTR) | Err(nix::errno::Errno::EAGAIN) => {
            // Having no data (EAGAIN) for nonblocking FDs
            // is unexpected due to use of poll; but can safely ignore this.
            // For EINTR, we could retry in this a loop, but instead will
            // return OK and let the main loop handle retrying for us
            Ok(())
        }
        Err(_) => Err("Error reading from socket"),
    }
}

fn write_to_socket(socket: &OwnedFd, bufs: &mut ProxySide) -> Result<(), &'static str> {
    // after writing, move bytes to front. todo: use a mutable-slice window?
    if bufs.nbytes_dst == 0 {
        panic!("assumed nonempty");
    }

    let nfds_sent = std::cmp::min(bufs.fds.len(), 16);
    let nbytes_sent = if nfds_sent < bufs.fds.len() {
        1
    } else {
        bufs.nbytes_dst
    };
    let iovs = [IoSlice::new(&bufs.buf_dst[..nbytes_sent])];
    let mut tmp: ArrayVec<i32, 16> = ArrayVec::new();
    for f in &bufs.fds[..nfds_sent] {
        tmp.push(f.as_raw_fd());
    }
    let cmsgs = [nix::sys::socket::ControlMessage::ScmRights(&tmp)];

    // todo later: parse into chunks
    let r = nix::sys::socket::sendmsg::<()>(
        socket.as_raw_fd(),
        &iovs,
        &cmsgs,
        nix::sys::socket::MsgFlags::empty(),
        None,
    );
    match r {
        Ok(s) => {
            // alt: move window up ; or rotate if using single-copy model
            bufs.buf_dst.copy_within(s..bufs.nbytes_dst, 0);
            bufs.nbytes_dst -= s;
            bufs.fds.drain(..nfds_sent).count();
            Ok(())
        }
        Err(nix::errno::Errno::EINTR) | Err(nix::errno::Errno::EAGAIN) => {
            // Having no space (EAGAIN) for nonblocking FDs
            // is unexpected due to use of poll; but can safely ignore this.
            // For EINTR, we could retry in this a loop, but instead will
            // return OK and let the main loop handle retrying for us
            Ok(())
        }
        Err(e) => {
            eprintln!("Error writing to socket {}", e);
            Err("Error writing to socket")
        }
    }
}
struct ProxySide<'a> {
    buf_src: &'a mut [u8],
    nbytes_src: usize,
    buf_dst: &'a mut [u8],
    nbytes_dst: usize,
    fds: ArrayVec<OwnedFd, 32>,
}

#[derive(Clone, Copy)]
struct UpstreamObject {
    alt: Option<DownstreamID>,
    intf: &'static WaylandInterface,
    version: u32,
}
#[derive(Clone, Copy)]
struct DownstreamObject {
    alt: Option<UpstreamID>,
    intf: &'static WaylandInterface,
    version: u32,
}

struct OutputInfo {
    name: Option<String>,
}

struct ProtocolState {
    // semi-stateless -- no per object state, just id remapping
    up_to_down: BTreeMap<UpstreamID, UpstreamObject>,
    down_to_up: BTreeMap<DownstreamID, DownstreamObject>,
    // this maps xdg-surface objects to the xdg-toplevel created from them
    surface_to_toplevel_map: BTreeMap<DownstreamID, DownstreamID>,
    toplevel_to_surface_map: BTreeMap<DownstreamID, DownstreamID>,
    upstream_max_id_no: u32,
    upstream_free_set: BTreeSet<UpstreamID>,
    /** Set of outputs provided by the compositor, plus metadata. Only tracked
     * if [.target_output] is Some. */
    outputs: BTreeMap<UpstreamID, OutputInfo>,
    /** The first wl_registry object that is created; will be reset to None once
     * the compositor deletes the object id for this registry */
    first_registry: Option<UpstreamID>,
    // empty region that must be created to turn pointer interactivity off
    empty_region: Option<UpstreamID>,
    layer: ZwlrLayerShellV1Layer,
    zone: i32,
    maximized: bool,
    kbd_interactive: bool,
    mouse_interactive: bool,
    /** If Some, the exact wl_output::name of the output to choose when making a layer-shell surface */
    target_output: Option<String>,
}

fn lookup_intf_by_name(name: &[u8]) -> Option<&'static WaylandInterface> {
    let r = ALL_INTERFACES.binary_search_by(|cand_intf| cand_intf.name.as_bytes().cmp(name));
    if let Ok(idx) = r {
        Some(ALL_INTERFACES[idx])
    } else {
        None
    }
}

fn get_new_upstream_client_id(state: &mut ProtocolState) -> UpstreamID {
    if let Some(x) = state.upstream_free_set.pop_first() {
        x
    } else {
        state.upstream_max_id_no += 1;
        UpstreamID(state.upstream_max_id_no)
    }
}
/* mark ID as being available */
fn delete_upstream_client_id(state: &mut ProtocolState, id: UpstreamID) {
    state.upstream_free_set.insert(id);
    while !state.upstream_free_set.is_empty() {
        let x = *state.upstream_free_set.iter().next_back().unwrap();
        if x.0 == state.upstream_max_id_no {
            state.upstream_free_set.remove(&x);
            state.upstream_max_id_no -= 1;
        } else {
            break;
        }
    }
}

fn map_id_unconditional(from_upstream: bool, state: &ProtocolState, id: u32) -> u32 {
    if from_upstream {
        state.up_to_down[&UpstreamID(id)]
            .alt
            .expect("no matching downstream object")
            .0
    } else {
        state.down_to_up[&DownstreamID(id)]
            .alt
            .expect("no matching upstream object")
            .0
    }
}

fn create_objects(
    msg: &[u8],
    parent_version: u32,
    meth: &WaylandMethod,
    from_upstream: bool,
    state: &mut ProtocolState,
) -> Result<(), ParseError> {
    let init_len = msg.len();
    let mut tail = &msg[8..];
    for op in meth.sig {
        match op {
            WaylandArgument::Uint | WaylandArgument::Int | WaylandArgument::Fixed => {
                parse_u32(&mut tail)?;
            }
            WaylandArgument::Fd => {
                // do nothing
            }
            WaylandArgument::Object(_) | WaylandArgument::GenericObject => {
                let id = parse_u32(&mut tail)?;
                if id != 0 {
                    // TODO: validate object ID
                }
            }
            WaylandArgument::NewId(new_intf) => {
                let id = parse_u32(&mut tail)?;

                let alt_id = if from_upstream {
                    // windowtolayer does not yet add or remove objects created by
                    // the compositor, like wl_data_offer/zwp_primary_selection_offer_v1
                    UpstreamID(id)
                } else {
                    get_new_upstream_client_id(state)
                };

                /* Note: this assumes the creating object exists both upstream and downstream,
                 * with the exact same version; otherwise this will not make sense */
                insert_object(
                    state,
                    Some((alt_id, new_intf, parent_version)),
                    Some((DownstreamID(id), new_intf, parent_version)),
                );
            }
            WaylandArgument::GenericNewId => {
                // order: (string, version, new_id)
                let string = parse_string(&mut tail)?.unwrap();
                let version = parse_u32(&mut tail)?;
                let id = DownstreamID(parse_u32(&mut tail)?);

                let new_intf = lookup_intf_by_name(string).expect("Unidentified object bound");

                let alt_id = get_new_upstream_client_id(state);
                assert!(!from_upstream);

                insert_object(
                    state,
                    Some((alt_id, new_intf, version)),
                    Some((id, new_intf, version)),
                );
            }
            WaylandArgument::String => {
                let _ = parse_string(&mut tail)?;
            }
            WaylandArgument::Array => {
                let _ = parse_array(&mut tail)?;
            }
        }
    }

    if !tail.is_empty() {
        println!(
            "Parse failure: only consumed {} of {} bytes",
            init_len - tail.len(),
            init_len
        );
        Err(ParseError(()))
    } else {
        Ok(())
    }
}

fn write_translate(
    msg: &[u8],
    meth: &WaylandMethod,
    from_upstream: bool,
    state: &ProtocolState,
    dst: &mut &mut [u8],
) -> Result<(), ParseError> {
    let mut tail = msg;
    let init_dst_len = dst.len();
    let object_id = parse_u32(&mut tail)?;
    let header = parse_u32(&mut tail)?;

    write_u32(dst, map_id_unconditional(from_upstream, state, object_id)).unwrap();
    write_u32(dst, header).unwrap();

    for op in meth.sig {
        match op {
            WaylandArgument::Uint | WaylandArgument::Int | WaylandArgument::Fixed => {
                let v = parse_u32(&mut tail)?;
                write_u32(dst, v).unwrap();
            }
            WaylandArgument::Fd => {
                // do nothing
            }
            WaylandArgument::Object(_)
            | WaylandArgument::GenericObject
            | WaylandArgument::NewId(_) => {
                let id = parse_u32(&mut tail)?;
                if id == 0 {
                    write_u32(dst, 0).unwrap();
                } else {
                    write_u32(dst, map_id_unconditional(from_upstream, state, id)).unwrap();
                }
            }
            WaylandArgument::GenericNewId => {
                // order: (string, version, new_id)
                let string = parse_string(&mut tail)?;
                let version = parse_u32(&mut tail)?;
                let id = parse_u32(&mut tail)?;
                write_string(dst, string).unwrap();
                write_u32(dst, version).unwrap();

                write_u32(dst, map_id_unconditional(from_upstream, state, id)).unwrap();
            }
            WaylandArgument::String => {
                let s = parse_string(&mut tail)?;
                write_string(dst, s).unwrap();
            }
            WaylandArgument::Array => {
                let a = parse_array(&mut tail)?;
                write_array(dst, a).unwrap();
            }
        }
    }
    assert!(init_dst_len - dst.len() == msg.len());
    Ok(())
}

pub fn escape_non_ascii_printable(name: &[u8]) -> String {
    use std::fmt::Write;

    let mut s = String::new();
    for c in name {
        match *c {
            b' '..=b'~' => s.push(char::from_u32(*c as u32).unwrap()),
            _ => {
                write!(s, "\\x{:02x}", *c).unwrap();
            }
        }
    }
    s
}

struct OptId(Option<u32>);
impl Display for OptId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if let Some(x) = self.0 {
            write!(f, "{}", x)
        } else {
            write!(f, "_")
        }
    }
}

fn log_message(from_upstream: bool, state: &ProtocolState, msg: &[u8], processed: bool) {
    let object_id = u32::from_le_bytes(msg[..4].try_into().unwrap());
    let header = u32::from_le_bytes(msg[4..8].try_into().unwrap());
    let opcode = header & 0xffff;
    let Some((upstream_id, downstream_id, intf)) = (if from_upstream != processed {
        state
            .up_to_down
            .get(&UpstreamID(object_id))
            .map(|x| (Some(object_id), x.alt.map(|y| y.0), x.intf))
    } else {
        state
            .down_to_up
            .get(&DownstreamID(object_id))
            .map(|x| (x.alt.map(|y| y.0), Some(object_id), x.intf))
    }) else {
        panic!(
            "Failed to look up object id translation: input {}, from_upstream {}, processed {}",
            object_id, from_upstream, processed,
        );
    };
    let meth: &WaylandMethod = if from_upstream {
        &intf.evts[opcode as usize]
    } else {
        &intf.reqs[opcode as usize]
    };

    debug!(
        "{}{} {}#({},{}).{}({})",
        if processed { "send" } else { "recv" },
        if from_upstream { "" } else { " ->" },
        intf.name,
        OptId(upstream_id),
        OptId(downstream_id),
        meth.name,
        MethodArguments::new(meth, msg),
    );
}
fn log_messages(from_upstream: bool, state: &ProtocolState, mut msgs: &[u8], processed: bool) {
    while msgs.len() >= 8 {
        let oheader = u32::from_le_bytes(msgs[4..8].try_into().unwrap());
        let obyte_len = (oheader >> 16) as usize;
        assert!(msgs.len() >= obyte_len);
        log_message(from_upstream, state, &msgs[..obyte_len], processed);
        msgs = &msgs[obyte_len..];
    }
    assert!(msgs.is_empty());
}

struct QuotedStrings<'a>(Vec<&'a str>);
impl Display for QuotedStrings<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for (i, x) in self.0.iter().enumerate() {
            if i > 0 {
                write!(f, " ")?;
            }
            write!(f, "\"{}\"", x)?;
        }
        Ok(())
    }
}

fn insert_object(
    state: &mut ProtocolState,
    upstream: Option<(UpstreamID, &'static WaylandInterface, u32)>,
    downstream: Option<(DownstreamID, &'static WaylandInterface, u32)>,
) {
    if let Some((uid, uintf, uver)) = upstream {
        if state
            .up_to_down
            .insert(
                uid,
                UpstreamObject {
                    alt: downstream.map(|x| x.0),
                    intf: uintf,
                    version: uver,
                },
            )
            .is_some()
        {
            // panic!("ID collision at {} for upstream object", uid);
        }
    }
    if let Some((did, dintf, dver)) = downstream {
        if state
            .down_to_up
            .insert(
                did,
                DownstreamObject {
                    alt: upstream.map(|x| x.0),
                    intf: dintf,
                    version: dver,
                },
            )
            .is_some()
        {
            // panic!("ID collision at {} for downstream object", did);
        }
    }
}

/** A problem with the messages received from the Wayland client or compositor. */
enum WaylandError {
    Parse,
    Other(String),
}

impl From<ParseError> for WaylandError {
    fn from(_value: ParseError) -> Self {
        Self::Parse
    }
}

/** Result of processing a message: if not Done, indicates what to wait for. */
enum ProcResult {
    Done,
    NeedsSpace,
    WaitForOtherDirection,
}
fn process_event(
    state: &mut ProtocolState,
    msg: &[u8],
    dst: &mut &mut [u8],
    reverse_dst: &mut &mut [u8],
) -> Result<ProcResult, WaylandError> {
    use ProcResult::*;
    let object_id = UpstreamID(u32::from_le_bytes(msg[..4].try_into().unwrap()));
    let header = u32::from_le_bytes(msg[4..8].try_into().unwrap());
    let opcode = header & 0xffff;

    let res = state.up_to_down.get(&object_id).copied();
    if res.is_none() {
        panic!(
            "Failed to look up object id translation for event: input {}",
            object_id,
        );
        // todo: return Err(), maybe, since this often implies invalid Wayland messages?
    }
    let UpstreamObject {
        alt: alt_id,
        intf,
        version,
    } = res.unwrap();

    let meth: &WaylandMethod = &intf.evts[opcode as usize];

    if intf.uid == WL_REGISTRY.uid && opcode == (WlRegistryEvtIDs::Global as u32) {
        let (name, string, version) = parse_wl_registry_evt_global(msg)?;
        let string = string.unwrap();
        let opt_glob_intf = lookup_intf_by_name(string);

        if string.eq(XDG_WM_BASE.name.as_bytes()) {
            // do nothing, thereby filtering out the global
            Ok(Done)
        } else if string.eq(ZWLR_LAYER_SHELL_V1.name.as_bytes()) {
            /* replace upstream layer-shell with matching downstream xdg-wm-base */
            let new_name = XDG_WM_BASE.name.as_bytes();
            let length = length_wl_registry_evt_global(Some(new_name));
            if dst.len() < length {
                return Ok(NeedsSpace);
            }

            write_wl_registry_evt_global(
                dst,
                alt_id.unwrap(),
                name,
                Some(new_name),
                XDG_WM_BASE.version,
            );
            Ok(Done)
        } else if let Some(glob_intf) = opt_glob_intf {
            let length = length_wl_registry_evt_global(Some(string));
            let bind_output = if string.eq(WL_OUTPUT.name.as_bytes())
                && state.target_output.is_some()
                && state.first_registry == Some(object_id)
            {
                if version < 4 {
                    eprintln!(
                        "wl_output global with name={} was advertised with a version {} < 4 and \
                        its name cannot be detected, so picking an output with a \
                        matching name will probably fail",
                        name, version
                    );
                }
                version >= 4
            } else {
                false
            };

            let rev_length = if bind_output {
                length_wl_registry_req_bind(WL_OUTPUT.name.as_bytes())
            } else {
                0
            };
            if dst.len() < length {
                return Ok(NeedsSpace);
            }
            if reverse_dst.len() < rev_length {
                /* cannot write message, not enough space to inject reverse direction message */
                return Ok(NeedsSpace);
            }

            write_wl_registry_evt_global(
                dst,
                alt_id.unwrap(),
                name,
                Some(string),
                std::cmp::min(version, glob_intf.version),
            );
            if bind_output {
                /* As the client is not guaranteed to bind any output, and getting output information
                 * requires a roundtrip, immediately bind all outputs ourselves as soon as possible
                 * to ensure wl_output::name and wl_output::done can possibly arrive, and the correct
                 * output to attach the layer surface to, can be identified, before an xdg_toplevel
                 * is made */
                // TODO: if the Wayland client later binds the output, redirect it to this
                // object, and later filter out object deletion; this will reduce the number
                // of repeated events the compositor must send for property updates and surface enter/leave
                let output_id = get_new_upstream_client_id(state);
                let output_version = 4;
                insert_object(state, Some((output_id, &WL_OUTPUT, output_version)), None);
                // todo: this requires that the registry has not been deleted
                write_wl_registry_req_bind(
                    reverse_dst,
                    object_id,
                    name,
                    string,
                    output_version,
                    output_id,
                );

                state.outputs.insert(output_id, OutputInfo { name: None });
            }
            Ok(Done)
        } else {
            /* Drop global, unrecognized */
            Ok(Done)
        }
    } else if intf.uid == WL_OUTPUT.uid && alt_id.is_none() {
        /* Events directed solely toward wl_outputs created by this program */
        if let WlOutputEvtIDs::Name = parse_wl_output_evt_ids(opcode).unwrap() {
            let name = parse_wl_output_evt_name(msg)?;
            let mut tmp: Option<String> = Some(std::str::from_utf8(name.unwrap()).unwrap().into());
            std::mem::swap(
                &mut state.outputs.get_mut(&object_id).unwrap().name,
                &mut tmp,
            );
        }
        Ok(Done)
    } else if intf.uid == WL_SURFACE.uid {
        /* For each created output, wl_surface::enter/::leave events are sent
         * corresponding to it; drop them */
        let drop = match parse_wl_surface_evt_ids(opcode).unwrap() {
            WlSurfaceEvtIDs::Enter => {
                let output = parse_wl_surface_evt_enter(msg)?;
                state.up_to_down.get(&output).unwrap().alt.is_none()
            }
            WlSurfaceEvtIDs::Leave => {
                let output = parse_wl_surface_evt_leave(msg)?;
                state.up_to_down.get(&output).unwrap().alt.is_none()
            }
            _ => false,
        };

        if !drop {
            if dst.len() < msg.len() {
                return Ok(NeedsSpace);
            }
            create_objects(msg, version, meth, true, state)?;
            write_translate(msg, meth, true, state, dst)?;
        }
        Ok(Done)
    } else if intf.uid == ZWLR_LAYER_SHELL_V1.uid {
        /* Do nothing: layer shell has no events */
        Ok(Done)
    } else if intf.uid == ZWLR_LAYER_SURFACE_V1.uid {
        match parse_zwlr_layer_surface_v1_evt_ids(opcode).unwrap() {
            ZwlrLayerSurfaceV1EvtIDs::Configure => {
                let xdg_surface_id = alt_id.unwrap();
                let toplevel_id = state.surface_to_toplevel_map[&xdg_surface_id];
                let toplevel_version = state.down_to_up.get(&toplevel_id).unwrap().version;

                let mut states = Vec::new();
                states.extend_from_slice(&u32::to_le_bytes(XdgToplevelState::Fullscreen as u32));
                if state.maximized {
                    states.extend_from_slice(&u32::to_le_bytes(XdgToplevelState::Maximized as u32));
                }
                let mut length =
                    length_xdg_surface_evt_configure() + length_xdg_toplevel_evt_configure(&states);
                // todo: after the first time, only send configure_bounds/wm_capabilities when there is a change
                if toplevel_version >= 4 {
                    length += length_xdg_toplevel_evt_configure_bounds();
                }
                let capabilities = &[]; /* no window menu, maximize, fullscreen, or minimize controls available */
                if toplevel_version >= 5 {
                    length += length_xdg_toplevel_evt_wm_capabilities(capabilities);
                }

                if dst.len() < length {
                    return Ok(NeedsSpace);
                }

                let (serial, width, height) = parse_zwlr_layer_surface_v1_evt_configure(msg)?;

                if toplevel_version >= 5 {
                    write_xdg_toplevel_evt_wm_capabilities(dst, toplevel_id, capabilities);
                }
                if toplevel_version >= 4 {
                    let (bound_w, bound_h) = if state.maximized {
                        /* Note: when maximized, client needs to exactly use configured size,
                         * so this technically isn't required. */
                        (width.try_into().unwrap(), height.try_into().unwrap())
                    } else {
                        /* No constraint */
                        (0, 0)
                    };
                    write_xdg_toplevel_evt_configure_bounds(dst, toplevel_id, bound_w, bound_h);
                }
                write_xdg_toplevel_evt_configure(
                    dst,
                    toplevel_id,
                    width.try_into().unwrap(),
                    height.try_into().unwrap(),
                    &states,
                );
                write_xdg_surface_evt_configure(dst, xdg_surface_id, serial);
            }
            ZwlrLayerSurfaceV1EvtIDs::Closed => {
                // todo: implement
            }
        }
        Ok(Done)
    } else if intf.uid == WL_DISPLAY.uid && opcode == (WlDisplayEvtIDs::DeleteId as u32) {
        let length = length_wl_display_evt_delete_id();
        if dst.len() < length {
            return Ok(NeedsSpace);
        }

        let del_id = UpstreamID(parse_wl_display_evt_delete_id(msg)?);
        let ds_id = *state.up_to_down.get(&del_id).unwrap();
        // todo: remove entries in up_to_down/down_to_up tables promptly,
        // instead of waiting to overwrite them

        if let Some(u) = state.first_registry {
            if u == del_id {
                state.first_registry = None;
            }
        }
        delete_upstream_client_id(state, del_id);

        if let Some(x) = ds_id.alt {
            write_wl_display_evt_delete_id(dst, alt_id.unwrap(), x.0);
        }
        Ok(Done)
    } else {
        if dst.len() < msg.len() {
            return Ok(NeedsSpace);
        }
        create_objects(msg, version, meth, true, state)?;
        write_translate(msg, meth, true, state, dst)?;
        Ok(Done)
    }
}
fn process_request(
    state: &mut ProtocolState,
    msg: &[u8],
    dst: &mut &mut [u8],
    _reverse_dst: &mut &mut [u8],
) -> Result<ProcResult, WaylandError> {
    use ProcResult::*;
    let object_id = DownstreamID(u32::from_le_bytes(msg[..4].try_into().unwrap()));
    let header = u32::from_le_bytes(msg[4..8].try_into().unwrap());
    let opcode = header & 0xffff;

    /* Process message */
    let res = state.down_to_up.get(&object_id).copied();
    if res.is_none() {
        panic!(
            "Failed to look up object id translation for request: input {}",
            object_id,
        );
        // todo: return Err(), maybe, since this often implies invalid Wayland messages?
    }
    let DownstreamObject {
        alt: alt_id,
        intf,
        version,
    } = res.unwrap();
    let meth: &WaylandMethod = &intf.reqs[opcode as usize];

    if intf.uid == WL_DISPLAY.uid && opcode == (WlDisplayReqIDs::GetRegistry as u32) {
        if dst.len() < msg.len() {
            return Ok(NeedsSpace);
        }
        let downstream_id = parse_wl_display_req_get_registry(msg)?;
        let upstream_id = get_new_upstream_client_id(state);

        insert_object(
            state,
            Some((upstream_id, &WL_REGISTRY, 1)),
            Some((downstream_id, &WL_REGISTRY, 1)),
        );
        if state.first_registry.is_none() {
            // Note: odd behavior is possible if the registry is destroyed later. It may be entirely
            // avoidable if a registry is kept alive indefinitely, either filtering wl_fixes requests
            // to never destroy the registry, or making a new registry object (which is wasteful).
            state.first_registry = Some(upstream_id);
        }
        write_wl_display_req_get_registry(dst, alt_id.unwrap(), upstream_id);
        Ok(Done)
    } else if intf.uid == WL_REGISTRY.uid && opcode == (WlRegistryReqIDs::Bind as u32) {
        let (name, string, downstream_version, id) = parse_wl_registry_req_bind(msg)?;
        let mut string = string.unwrap();

        let downstream_intf = lookup_intf_by_name(string).expect("Unidentified object bound");
        let (upstream_intf, upstream_version) = if string.eq(XDG_WM_BASE.name.as_bytes()) {
            string = ZWLR_LAYER_SHELL_V1.name.as_bytes();
            // version 3 needed to properly translate 'xdg_wm_base.destroy'
            (&ZWLR_LAYER_SHELL_V1, 3)
        } else {
            (downstream_intf, downstream_version)
        };

        let make_empty_region = !state.mouse_interactive
            && string == WL_COMPOSITOR.name.as_bytes()
            && state.empty_region.is_none();

        let length = length_wl_registry_req_bind(string)
            + (if make_empty_region {
                length_wl_compositor_req_create_region()
            } else {
                0
            });
        if dst.len() < length {
            return Ok(NeedsSpace);
        }
        let upstream_id = get_new_upstream_client_id(state);
        write_wl_registry_req_bind(
            dst,
            alt_id.unwrap(),
            name,
            string,
            upstream_version,
            upstream_id,
        );
        if make_empty_region {
            let reg_id = get_new_upstream_client_id(state);
            state.empty_region = Some(reg_id);
            write_wl_compositor_req_create_region(dst, upstream_id, reg_id);
            insert_object(state, Some((reg_id, &WL_REGION, upstream_version)), None);
        }

        insert_object(
            state,
            Some((upstream_id, upstream_intf, upstream_version)),
            Some((id, downstream_intf, downstream_version)),
        );
        Ok(Done)
    } else if intf.uid == WL_COMPOSITOR.uid
        && (opcode == WlCompositorReqIDs::CreateSurface as u32)
        && !state.mouse_interactive
    {
        let length =
            length_wl_compositor_req_create_surface() + length_wl_surface_req_set_input_region();
        if dst.len() < length {
            return Ok(NeedsSpace);
        }

        assert!(state.empty_region.is_some());
        let surf_id = parse_wl_compositor_req_create_surface(msg)?;
        let alt_surf_id = get_new_upstream_client_id(state);

        insert_object(
            state,
            Some((alt_surf_id, &WL_SURFACE, version)),
            Some((surf_id, &WL_SURFACE, version)),
        );

        write_wl_compositor_req_create_surface(dst, alt_id.unwrap(), alt_surf_id);
        write_wl_surface_req_set_input_region(dst, alt_surf_id, state.empty_region.unwrap());
        Ok(Done)
    } else if intf.uid == WL_SURFACE.uid
        && (opcode == WlSurfaceReqIDs::SetInputRegion as u32)
        && !state.mouse_interactive
    {
        // Drop all requests to control the input region, as in this case
        // it is being injected on surface creation
        Ok(Done)
    } else if intf.uid == ZXDG_DECORATION_MANAGER_V1.uid {
        match parse_zxdg_decoration_manager_v1_req_ids(opcode).unwrap() {
            ZxdgDecorationManagerV1ReqIDs::Destroy => { /* todo */ }
            ZxdgDecorationManagerV1ReqIDs::GetToplevelDecoration => {
                /* create a downstream only toplevel object, and link it to the xdg-surface */
                let (new_decoration_id, _xdg_toplevel_id) =
                    parse_zxdg_decoration_manager_v1_req_get_toplevel_decoration(msg)?;

                // TODO: associate the decoration with the surface

                /* this object has _no_ corresponding object on the other side */
                insert_object(
                    state,
                    None,
                    Some((new_decoration_id, &ZXDG_TOPLEVEL_DECORATION_V1, version)),
                );
            }
        }
        Ok(Done)
    } else if intf.uid == ZXDG_TOPLEVEL_DECORATION_V1.uid {
        /* Ignore all */
        Ok(Done)
    } else if intf.uid == XDG_WM_BASE.uid {
        match parse_xdg_wm_base_req_ids(opcode).unwrap() {
            XdgWmBaseReqIDs::GetXdgSurface => {
                let target_output_id = if let Some(ref name) = state.target_output {
                    let mut id = UpstreamID(0);
                    let mut complete = true;
                    for (output, val) in state.outputs.iter() {
                        if let Some(ref vname) = val.name {
                            if vname == name {
                                id = *output;
                                break;
                            }
                        } else {
                            complete = false;
                        }
                    }
                    if id != UpstreamID(0) {
                        id
                    } else if complete {
                        let mut output_list: Vec<&str> = state
                            .outputs
                            .values()
                            .map(|x| x.name.as_ref().unwrap().as_str())
                            .collect();
                        output_list.sort();

                        eprintln!(
                            "Output \"{}\" was not found, exiting. There {} {} {}: {}",
                            name,
                            if output_list.len() == 1 { "is" } else { "are" },
                            output_list.len(),
                            if output_list.len() == 1 {
                                "output"
                            } else {
                                "outputs"
                            },
                            QuotedStrings(output_list)
                        );
                        std::process::exit(1);
                    } else {
                        return Ok(WaitForOtherDirection);
                    }
                } else {
                    UpstreamID(0)
                };

                let namespace = &[];
                let length = length_zwlr_layer_shell_v1_req_get_layer_surface(Some(namespace))
                    + length_zwlr_layer_surface_v1_req_set_anchor()
                    + length_zwlr_layer_surface_v1_req_set_exclusive_zone()
                    + (if state.kbd_interactive {
                        length_zwlr_layer_surface_v1_req_set_keyboard_interactivity()
                    } else {
                        0
                    });
                if dst.len() < length {
                    return Ok(NeedsSpace);
                }

                // issue: need to track toplevel for xdg-surface
                // make a separate table?
                let (new_xdg_surface_id, wl_surface_id) =
                    parse_xdg_wm_base_req_get_xdg_surface(msg)?;

                let upstream_wl_surface_id = state.down_to_up[&wl_surface_id].alt.unwrap();

                let layer_surface_id = get_new_upstream_client_id(state);

                write_zwlr_layer_shell_v1_req_get_layer_surface(
                    dst,
                    alt_id.unwrap(),
                    layer_surface_id,
                    upstream_wl_surface_id,
                    target_output_id,
                    state.layer as u32,
                    Some(namespace),
                );
                let anchor = ZwlrLayerSurfaceV1Anchor::Left as u32
                    | ZwlrLayerSurfaceV1Anchor::Top as u32
                    | ZwlrLayerSurfaceV1Anchor::Right as u32
                    | ZwlrLayerSurfaceV1Anchor::Bottom as u32;
                write_zwlr_layer_surface_v1_req_set_anchor(dst, layer_surface_id, anchor);
                write_zwlr_layer_surface_v1_req_set_exclusive_zone(
                    dst,
                    layer_surface_id,
                    state.zone,
                );
                if state.kbd_interactive {
                    write_zwlr_layer_surface_v1_req_set_keyboard_interactivity(
                        dst,
                        layer_surface_id,
                        ZwlrLayerSurfaceV1KeyboardInteractivity::OnDemand as u32,
                    );
                }

                let layer_shell_version = state.up_to_down.get(&alt_id.unwrap()).unwrap().version;
                insert_object(
                    state,
                    Some((
                        layer_surface_id,
                        &ZWLR_LAYER_SURFACE_V1,
                        layer_shell_version,
                    )),
                    Some((new_xdg_surface_id, &XDG_SURFACE, version)),
                );
            }
            XdgWmBaseReqIDs::CreatePositioner => {
                panic!("Creating positioners not yet supported");
            }
            XdgWmBaseReqIDs::Pong => {
                panic!("Never sent ping to client, but it sent a pong");
            }
            XdgWmBaseReqIDs::Destroy => {
                let length = length_zwlr_layer_shell_v1_req_destroy();
                if dst.len() < length {
                    return Ok(NeedsSpace);
                }
                write_zwlr_layer_shell_v1_req_destroy(dst, alt_id.unwrap());
            }
        }
        Ok(Done)
    } else if intf.uid == XDG_SURFACE.uid {
        match parse_xdg_surface_req_ids(opcode).unwrap() {
            XdgSurfaceReqIDs::Destroy => { /* todo */ }
            XdgSurfaceReqIDs::GetToplevel => {
                /* create a downstream only toplevel object, and link it to the xdg-surface */
                let new_toplevel_id = parse_xdg_surface_req_get_toplevel(msg)?;

                state
                    .surface_to_toplevel_map
                    .insert(object_id, new_toplevel_id);
                state
                    .toplevel_to_surface_map
                    .insert(new_toplevel_id, object_id);

                /* this object has _no_ corresponding object on the other side */
                insert_object(state, None, Some((new_toplevel_id, &XDG_TOPLEVEL, version)));
            }
            XdgSurfaceReqIDs::GetPopup => {
                // technically, these can be forwarded
                panic!("Popups not implemented");
            }
            XdgSurfaceReqIDs::SetWindowGeometry => { /* ignore, for now */ }
            XdgSurfaceReqIDs::AckConfigure => {
                let length = length_zwlr_layer_surface_v1_req_ack_configure();
                if dst.len() < length {
                    return Ok(NeedsSpace);
                }
                let serial = parse_xdg_surface_req_ack_configure(msg)?;
                write_zwlr_layer_surface_v1_req_ack_configure(dst, alt_id.unwrap(), serial);
            }
        }
        Ok(Done)
    } else if intf.uid == XDG_TOPLEVEL.uid {
        if let XdgToplevelReqIDs::SetMaxSize = parse_xdg_toplevel_req_ids(opcode).unwrap() {
            if state.maximized {
                /* Accept whatever the compositor sends in response to the default
                 * layer shell surface size (which is 0,0 = compositor preference) */
                return Ok(Done);
            }

            /* Translate set_max_size requests to layer shell;
             * set_min_size requests have not equivalent and will be ignored */
            let length = length_zwlr_layer_surface_v1_req_set_size()
                + length_zwlr_layer_surface_v1_req_set_anchor();
            if dst.len() < length {
                return Ok(NeedsSpace);
            }

            /* xdg_toplevel::set_max_size and zwlr_layer_surface_v1::set_size have the
             * same 0=unspecified handling; note ::set_size requires the unbound dimensions
             * to be anchored to both sides */
            let (width, height) = parse_xdg_toplevel_req_set_max_size(msg)?;
            let xdg_surface_id = state.toplevel_to_surface_map[&object_id];
            let layer_surface_id = state.down_to_up.get(&xdg_surface_id).unwrap().alt.unwrap();
            write_zwlr_layer_surface_v1_req_set_size(
                dst,
                layer_surface_id,
                width.try_into().unwrap(),
                height.try_into().unwrap(),
            );
            /* Compositors differ in how they choose sizes in response to anchor=0 vs anchor=all
             * for wlr-layer-shell. Prefer centering by anchoring to nothing instead of to parallel
             * axes. However, ::set_size requires parallel anchors when a 0 value is given. */
            let mut anchor = 0;
            if width == 0 {
                anchor |= (ZwlrLayerSurfaceV1Anchor::Left as u32)
                    | (ZwlrLayerSurfaceV1Anchor::Right as u32);
            }
            if height == 0 {
                anchor |= (ZwlrLayerSurfaceV1Anchor::Top as u32)
                    | (ZwlrLayerSurfaceV1Anchor::Bottom as u32);
            }
            write_zwlr_layer_surface_v1_req_set_anchor(dst, layer_surface_id, anchor);
        }
        Ok(Done)
    } else {
        if dst.len() < msg.len() {
            return Ok(NeedsSpace);
        }
        create_objects(msg, version, meth, false, state)?;
        write_translate(msg, meth, false, state, dst)?;
        Ok(Done)
    }
}

fn process_message(
    from_upstream: bool,
    state: &mut ProtocolState,
    msg: &[u8],
    dst: &mut &mut [u8],
    reverse_dst: &mut &mut [u8],
) -> Result<ProcResult, WaylandError> {
    if from_upstream {
        process_event(state, msg, dst, reverse_dst)
    } else {
        process_request(state, msg, dst, reverse_dst)
    }
}

fn process_messages(
    from_upstream: bool,
    bufs: &mut ProxySide,
    reverse: &mut ProxySide,
    state: &mut ProtocolState,
) -> Result<(), String> {
    // TODO: use a ring-type data structure to make zero-copying possible; to handle
    // long messages, use 100% overhang
    let mut ncopied = 0;
    let mut remaining = bufs.nbytes_src;
    while remaining >= 8 {
        // Steal first message
        let header = u32::from_le_bytes(bufs.buf_src[ncopied + 4..ncopied + 8].try_into().unwrap());
        let byte_len = (header >> 16) as usize;

        if remaining < byte_len {
            // Partial message, do nothing.
            break;
        }
        let msg = &bufs.buf_src[ncopied..ncopied + byte_len];
        let mut dst: &mut [u8] = &mut bufs.buf_dst[bufs.nbytes_dst..];
        let mut reverse_dst: &mut [u8] = &mut reverse.buf_dst[reverse.nbytes_dst..];
        let dst_len = dst.len();
        let rev_dst_len = reverse_dst.len();
        if log::log_enabled!(log::Level::Debug) {
            log_message(from_upstream, state, msg, false);
        }

        match process_message(from_upstream, state, msg, &mut dst, &mut reverse_dst).map_err(
            |e| match e {
                WaylandError::Parse => "Failed to parse messsage".into(),
                WaylandError::Other(x) => x,
            },
        )? {
            ProcResult::NeedsSpace => {
                debug!("Waiting for output space");
                break;
            }
            ProcResult::WaitForOtherDirection => {
                // TODO: handle this in the main loop to avoid busy-waiting if the
                // compositor takes a long time to send output name events
                debug!("Waiting for progress on the other direction");
                break;
            }
            ProcResult::Done => (),
        }
        let new_dst_len = dst.len();
        let new_rev_dst_len = reverse_dst.len();
        if log::log_enabled!(log::Level::Debug) {
            log_messages(
                from_upstream,
                state,
                &bufs.buf_dst[bufs.nbytes_dst..bufs.nbytes_dst + (dst_len - new_dst_len)],
                true,
            );
            log_messages(
                !from_upstream,
                state,
                &reverse.buf_dst
                    [reverse.nbytes_dst..reverse.nbytes_dst + (rev_dst_len - new_rev_dst_len)],
                true,
            );
        }
        bufs.nbytes_dst += dst_len - new_dst_len;
        reverse.nbytes_dst += rev_dst_len - new_rev_dst_len;

        // Mark message as having been read
        ncopied += byte_len;
        remaining -= byte_len;
    }

    bufs.buf_src.copy_within(ncopied..bufs.nbytes_src, 0);
    bufs.nbytes_src -= ncopied;

    Ok(())
}

const USAGE: &str = r"Usage: windowtolayer [OPTIONS] <command>...

Arguments:
  <command>...  Command to run

Options:
  -l, --layer <L>           Set the layer at which to display the application
                            [default: background] [possible values: background, bottom, top, overlay]
  -i, --interactivity <I>   What type of interaction is permitted [default: none]
                            [possible values: none, pointer, keyboard, all]
  -z, --exclusive-zone <Z>  Set the layer surface exclusive zone value [default: -1]
  -m, --maximized           Notify application that it is maximized (must fill entire output)
  --output-name <name>      Choose the name of the output to use; by default the compositor decides.
  -d, --debug               Log debug messages
  -h, --help                Print help
  -V, --version             Print version";

const VERSION: &str = env!("CARGO_PKG_VERSION");

const ON_USAGE_ERROR: &str = "For more information, see `windowtolayer --help`";

macro_rules! command_line_error {
    ($x:tt) => {
        command_line_error_start();
        eprintln!($x);
        command_line_error_end();
    };
    ($x:tt, $($arg:tt)+) => {
        command_line_error_start();
        eprintln!($x, $($arg)+);
        command_line_error_end();
    };
}

#[cold]
fn command_line_error_start() {
    eprintln!(
        "In command line arguments: {:?}",
        std::env::args_os().collect::<Vec<OsString>>()
    );
}
fn command_line_error_end() -> ! {
    eprintln!("{}", ON_USAGE_ERROR);
    std::process::exit(1);
}

fn check_no_option(parser: &mut lexopt::Parser, argument: &str) {
    if let Some(v) = parser.optional_value() {
        command_line_error!("Unexpected optional value {:?} for {}", v, argument);
    }
}

fn get_option(parser: &mut lexopt::Parser, argument: &str) -> OsString {
    if let Ok(v) = parser.value() {
        v
    } else {
        command_line_error!("Missing argument for {}", argument);
    }
}

fn main() {
    let mut debug = false;
    let (mut kbd_int, mut mouse_int) = (false, false);
    let mut layer: ZwlrLayerShellV1Layer = ZwlrLayerShellV1Layer::Background;
    let mut maximized = false;
    let mut target_output: Option<String> = None;
    let mut zone: i32 = -1;
    let mut command: Vec<&OsStr> = Vec::new();

    /* Use lexopt here, instead of clap, to save ~200-400kB of disk space (which at 100MB/sec
     * disk speed, amount to 2-4 ms of cold startup time.). (On a reasonably but not extremely
     * fast machine with an SSD, a 700kB executable with lexopt took in 5.8 msec to evaluate
     * --help, while a 1000kB executable using clap took 8.1 msec.) */
    let mut parser = lexopt::Parser::from_env();
    let mut first_val: Option<OsString> = None;
    while let Ok(Some(arg)) = parser.next() {
        match arg {
            Arg::Short('h') | Arg::Long("help") => {
                if matches!(arg, Arg::Long(_)) {
                    check_no_option(&mut parser, "--help");
                }
                println!("{}", USAGE);
                return;
            }
            Arg::Short('V') | Arg::Long("version") => {
                if matches!(arg, Arg::Long(_)) {
                    check_no_option(&mut parser, "--version");
                }
                println!("{}", VERSION);
                return;
            }
            Arg::Short('m') | Arg::Long("maximized") => {
                if matches!(arg, Arg::Long(_)) {
                    check_no_option(&mut parser, "--maximized");
                }
                maximized = true;
            }
            Arg::Short('d') | Arg::Long("debug") => {
                if matches!(arg, Arg::Long(_)) {
                    check_no_option(&mut parser, "--debug");
                }
                debug = true;
            }
            Arg::Long("output-name") => {
                target_output = if let Ok(y) =
                    get_option(&mut parser, "--output-name").into_string()
                {
                    Some(y)
                } else {
                    command_line_error!("Argument for --output-name is not a valid UTF-8 string");
                }
            }
            Arg::Short('z') | Arg::Long("exclusive_zone") | Arg::Long("exclusive-zone") => {
                let s = get_option(&mut parser, "--exclusive-zone");
                zone = if let Ok(y) = s.parse::<i32>() {
                    y
                } else {
                    command_line_error!("Argument {:?} for --exclusive-zone could not be parsed as an integer or is out of range", s);
                }
            }
            Arg::Short('i') | Arg::Long("interactivity") => {
                let s = get_option(&mut parser, "--interactivity");
                (kbd_int, mouse_int) = match s.to_str() {
                    Some("none") => (false, false),
                    Some("pointer") => (false, true),
                    Some("keyboard") => (true, false),
                    Some("all") => (true, true),
                    _ => {
                        command_line_error!("Argument {:?} for --interactivity is not in the list [none, pointer, keyboard, all]", s);
                    }
                };
            }
            Arg::Short('l') | Arg::Long("layer") => {
                let s = get_option(&mut parser, "--layer");
                layer = match s.to_str() {
                    Some("background") => ZwlrLayerShellV1Layer::Background,
                    Some("bottom") => ZwlrLayerShellV1Layer::Bottom,
                    Some("top") => ZwlrLayerShellV1Layer::Top,
                    Some("overlay") => ZwlrLayerShellV1Layer::Overlay,
                    _ => {
                        command_line_error!("Argument {:?} for --layer is not in the list [background, bottom, top, overlay]", s);
                    }
                };
            }

            Arg::Long(l) => {
                command_line_error!("Invalid long argument --{}", l);
            }
            Arg::Short(c) => {
                command_line_error!("Invalid short argument -{}", c);
            }

            Arg::Value(v) => {
                first_val = Some(v);
                break;
            }
        }
    }
    if let Some(ref v) = first_val {
        command.push(v.as_os_str());
    }

    let trailing_args = parser.raw_args();
    match trailing_args {
        Err(_) => {
            command_line_error!("Leftover unhandled argument");
        }
        Ok(ref x) => {
            for y in x.as_slice() {
                command.push(y.as_os_str());
            }
        }
    };
    if command.is_empty() {
        command_line_error!("Missing required argument <command>");
    }

    log::set_max_level(if debug {
        log::LevelFilter::Debug
    } else {
        log::LevelFilter::Error
    });
    log::set_logger(&LOGGER).unwrap();

    let conn_result = connect_to_upstream();
    let upstream_fd: OwnedFd = match conn_result {
        Ok(x) => x,
        Err(y) => {
            eprintln!("{}", y);
            return;
        }
    };

    let (downstream_fd, inherited_fd) = nix::sys::socket::socketpair(
        nix::sys::socket::AddressFamily::Unix,
        nix::sys::socket::SockType::Stream,
        None,
        SockFlag::SOCK_NONBLOCK,
    )
    .unwrap();
    set_cloexec(&downstream_fd, true);
    set_cloexec(&inherited_fd, false);
    let fd_str = inherited_fd.as_raw_fd().to_string();
    debug!(
        "Spawning program {:?} with args: {:?}",
        command[0],
        &command[1..]
    );
    /* WAYLAND_SOCKET should have priority over WAYLAND_DISPLAY,
     * so the command should connect to the proxy. If the command
     * incorrectly picks WAYLAND_DISPLAY, it may render as a normal window,
     * and recursively spawned programs may break if the command
     * does not clear WAYLAND_SOCKET. */
    let mut handle = Command::new(command[0])
        .args(&command[1..])
        .env("WAYLAND_SOCKET", fd_str)
        .spawn()
        .expect("Failed to run command");
    drop(inherited_fd);

    // Step 3: forward messages in each direction
    let mut bufs_upward = ProxySide {
        buf_src: &mut [0_u8; 4096],
        buf_dst: &mut [0_u8; 4096],
        fds: ArrayVec::new(),
        nbytes_src: 0,
        nbytes_dst: 0,
    };
    let mut bufs_downward = ProxySide {
        buf_src: &mut [0_u8; 4096],
        buf_dst: &mut [0_u8; 4096],
        fds: ArrayVec::new(),
        nbytes_src: 0,
        nbytes_dst: 0,
    };
    let mut state = ProtocolState {
        up_to_down: BTreeMap::new(),
        down_to_up: BTreeMap::new(),
        surface_to_toplevel_map: BTreeMap::new(),
        toplevel_to_surface_map: BTreeMap::new(),
        upstream_max_id_no: 1,
        upstream_free_set: BTreeSet::new(),
        outputs: BTreeMap::new(),
        first_registry: None,
        layer,
        zone,
        kbd_interactive: kbd_int,
        mouse_interactive: mouse_int,
        maximized,
        empty_region: None,
        target_output,
    };
    insert_object(
        &mut state,
        Some((UpstreamID(1), &WL_DISPLAY, 1)),
        Some((DownstreamID(1), &WL_DISPLAY, 1)),
    );

    loop {
        let writing_up = bufs_upward.nbytes_dst > 0 || !bufs_upward.fds.is_empty();
        let writing_down = bufs_downward.nbytes_dst > 0 || !bufs_downward.fds.is_empty();
        let mut pfds = [
            nix::poll::PollFd::new(
                downstream_fd.as_fd(),
                if writing_down {
                    nix::poll::PollFlags::POLLOUT
                } else {
                    nix::poll::PollFlags::POLLIN
                },
            ),
            nix::poll::PollFd::new(
                upstream_fd.as_fd(),
                if writing_up {
                    nix::poll::PollFlags::POLLOUT
                } else {
                    nix::poll::PollFlags::POLLIN
                },
            ),
        ];
        match nix::poll::poll(&mut pfds, nix::poll::PollTimeout::NONE) {
            Ok(_) => {}
            Err(e) => {
                eprintln!("Poll error {}", e);
                continue;
            }
        };

        let down_evts = pfds[0].revents().unwrap();
        let up_evts = pfds[1].revents().unwrap();

        if down_evts.contains(nix::poll::PollFlags::POLLHUP) {
            debug!("Downstream hang up");
            break;
        } else if down_evts.contains(nix::poll::PollFlags::POLLERR) {
            debug!("Downstream error up");
            break;
        } else if writing_down && down_evts.contains(nix::poll::PollFlags::POLLOUT) {
            if let Err(x) = write_to_socket(&downstream_fd, &mut bufs_downward) {
                eprintln!("{}", x);
                break;
            }
            // Process messages in the src buffer which were waiting to be translated
            // to the dst buffer (but which did not have enough free space)
            if let Err(x) = process_messages(true, &mut bufs_downward, &mut bufs_upward, &mut state)
            {
                eprintln!("{}", x);
                break;
            }
        } else if !writing_down && !writing_up && down_evts.contains(nix::poll::PollFlags::POLLIN) {
            if let Err(x) = read_from_socket(&downstream_fd, &mut bufs_upward) {
                eprintln!("{}", x);
                break;
            }
            if let Err(x) =
                process_messages(false, &mut bufs_upward, &mut bufs_downward, &mut state)
            {
                eprintln!("{}", x);
                break;
            }
        }

        if up_evts.contains(nix::poll::PollFlags::POLLHUP) {
            debug!("Upstream hang up");
            break;
        } else if up_evts.contains(nix::poll::PollFlags::POLLERR) {
            debug!("Upstream error up");
            break;
        } else if writing_up && up_evts.contains(nix::poll::PollFlags::POLLOUT) {
            if let Err(x) = write_to_socket(&upstream_fd, &mut bufs_upward) {
                eprintln!("{}", x);
                break;
            }
            // Process messages in the src buffer which were waiting to be translated
            // to the dst buffer (but which did not have enough free space)
            if let Err(x) =
                process_messages(false, &mut bufs_upward, &mut bufs_downward, &mut state)
            {
                eprintln!("{}", x);
                break;
            }
        } else if !writing_up && !writing_down && up_evts.contains(nix::poll::PollFlags::POLLIN) {
            if let Err(x) = read_from_socket(&upstream_fd, &mut bufs_downward) {
                eprintln!("{}", x);
                break;
            }
            if let Err(x) = process_messages(true, &mut bufs_downward, &mut bufs_upward, &mut state)
            {
                eprintln!("{}", x);
                break;
            }
        }
    }

    debug!("Waiting for program to exit");
    let _ = handle.try_wait();
    debug!("Done");
}
